# srprism_snakemake

This is a Snakemake pipline to run SRPRISM at the CDC

There are several steps to complete the process. They are as follow:
1. Obtain SRA accession numbers using google BigQuery (run_pipeline.sh)
2. Extract SRA accession numbers and add them to config.yaml file to be used by snakemake workflow. The script titled `write_config.py` does these extraction process for you.
3. Run SRPRISM pipeline:

    - Retrieve SRA data using PREFETCH
    - Run SRPRISM on data
    - Rank hits using ranking function

To run the snakefile on the CDC HPC the following command is recommended: `snakemake --latency-wait 60 --rerun-incomplete --keep-going -j 4 --cores 4 --cluster "qsub -V -cwd -o output-files-srprism1/ -e output-files-srprism1/ -pe smp 4 -M 'youremail@cdc.gov' -m ae -q all.q -N SRPRISM2 -S /bin/bash"`. 

The entire process should be able to be completed using run_pipeline.sh (`bash run_pipeline.sh`). 
