#!/bin/bash

docker run --rm --volumes-from gcloud-config google/cloud-sdk:slim bq query --format csv --nouse_legacy_sql 'select acc, biosample, bioproject, releasedate, librarylayout, mbases, organism FROM nih-sra-datastore.sra.metadata WHERE platform = "ILLUMINA" AND librarysource = "METAGENOMIC" AND consent = "public" AND assay_type = "WGS" AND libraryselection = "RANDOM"' | tail > last_10.txt

python write_config.py
