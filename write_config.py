import yaml
import pandas as pd

df = pd.read_csv("last_10.txt", header=None)

d = {}
for i in df[0]:
    d[i] = i

samples = {"samples":d}
samples["db"] = "/scicomp/home-pure/ncbi-readfinder/reference"
samples["tmp"] = "/scicomp/home-pure/ncbi-readfinder/tmp"

with open('config.yaml', 'w') as yaml_file:
    yaml.dump(samples, yaml_file, default_flow_style=False)
