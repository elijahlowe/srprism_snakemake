#!/bin/bash

configfile: "config.yaml"


tmp:config['tmp'] #'/scicomp/home-pure/ncbi-readfinder/tmp'


rule all:
    input:
        expand("{sra}.ranks", sra=config["samples"])

rule ranking:
    input:
        "{sra}.out.gz"
    output:
        "{sra}.ranks"
    params:
        db=config["db"],
        sra="{sra}"
    shell:
        """
        module load gcc/9.2.0
        zcat {input} | ranks_sam_normalized {params.db}/contigs.inf stdin | awk -v sra={params.sra} '{{printf("%s\\t%s\\n", sra, $0 )}}' > {output}
        """

rule srprism:
    input:
        config["tmp"]+"/{sra}" #expand("{tmp}/{sra}", tmp=TMP, sra=SRA)
    output:
        "{sra}.out.gz"
    params:
        db=config["db"],
        tmp=config["tmp"]
    shell:
        """
        module load gcc/9.2.0
        srprism search --trace-level info -m partial -I {params.db}/ref_srprism --memory 7168 -n 15 --sa-start 1 --sa-end 100 -R 4096 -r 250 -p false -i {input} -F sra -T {params.tmp} | cut -f 1-6 | gzip > {output}
        """


rule PREFETCH:
    params:
        "{sra}"
    output:
        temp(config["tmp"]+"/{sra}")
    shell:
        """
        module load gcc/9.2.0
        prefetch -o {output} -X 2000G {params}
        """
